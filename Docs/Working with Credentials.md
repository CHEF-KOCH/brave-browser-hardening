## [Passwords and Credentials](#passwords-and-credentials)

- Do not store credentials in your Browser, ever. The [reason (security.stackexchange.com)](https://security.stackexchange.com/questions/170481/how-secure-is-chrome-storing-a-password) is that Chromium stores the database password insecurely and it the database is considerable easy to decrypt with e.g. freeware tools from [Nirsoft (nirsoft.net)](https://www.nirsoft.net/utils/web_browser_password.html).
- Assuming you use Sync, do not enable password sync.
- Use Password Manager such as KeePass or Bitwarden that are more resilient against GPU brute-forcing attacks, ram hijacking and clipboard ex-filtration attacks.
- Forcing an expiration date for passwords is [not anymore recommend (ncsc.gov.uk)](hhttps://www.ncsc.gov.uk/blog-post/problems-forcing-regular-password-expiry), instead use a strong password that also can be generated trough Password Managers.
- Check your passwords and databases against HaveIbeenpwned and other services, some Password Managers have integrated mechanism to do so and automatically warn you or plugins to do this.
